﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VarAndDyanamicKeyword
{
    class Program
    {
        static void Main(string[] args)
        {
            ///-Var------------------------------------------------------------------------
            // In C#, an anonymous type is a type (class) without any name that can contain public read-only properties only.
            //   It cannot contain other members, such as fields, methods, events, etc.
            // var a = null can not assign null.
            //implicitly typed variable
            //u can not defined on class level .u define on local level or within method
            //Implicitly typed local variables can be used as a local variable in a function, 
            //in foreach, and for loop, as an anonymous type, in LINQ query expression, in using statement etc
            var a = 10;
            a = 20;
            //Dyanamic ------------------------------
            //Dyanamic keyword allowed  assign to null
            //you take properties
            //A method can have parameters of the dynamic type.
            dynamic b = 20;
            b = 10;
            b = null;

        }



    }
    public class DyanamicClass
    {
        public dynamic thy { get; set; }
        dynamic c;
        static dynamic nk;
        
        public void xyz(dynamic xyzw)
        {

            c = 10;
        }
    }
    public class VarClass
    {
        //public var MyProperty { get; set; }
        public void xyz()
        {
            
            var a = 10;
           // a = "";
        }

    }
}
